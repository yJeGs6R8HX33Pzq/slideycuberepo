using UnityEngine;

[CreateAssetMenu(menuName = "Database", fileName = "Instance")]
public class Database : ScriptableObject
{
    public Data[] datum;
}
