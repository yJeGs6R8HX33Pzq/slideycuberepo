using UnityEngine;

[CreateAssetMenu(menuName = "LevelType", fileName = "Instance")]
public class Data : ScriptableObject
{
    public int id;
    public string difficulty;

    [TextArea]
    public string description;

    public string songName;
    public AudioClip song;

    public Color defaultColor_1;
    public Color defaultColor_2;

    public RangeDefinition speed;

    [Header("Platform Contents")]
    public float heartProbability;
    public float spikeProbability;

    [Header("Platform Type")]
    public float vanishingProbability;
    public float movingProbability;
    public float megaProbability;


    public int maxDelta;

    public int goal;
}

//range definition used to define start and end point for interpolating values within a level
//start and end represent minimum and maximum values
[System.Serializable]
public struct RangeDefinition
{
    public float start;
    public float end;
}
