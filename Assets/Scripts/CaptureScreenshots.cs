using UnityEngine;
using System.IO;
using System;
public class CaptureScreenshots : MonoBehaviour
{
    public bool isCapturing;

    string folderPath;

    private void Start()
    {
        folderPath = Directory.GetCurrentDirectory() + "/Screenshots/";

        if (!System.IO.Directory.Exists(folderPath))
            System.IO.Directory.CreateDirectory(folderPath);

    }
    private void Update()
    {
        if (isCapturing)
        {
            var screenshotName =
                        "Screenshot_" +
                        System.DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") +
                        ".png";

            ScreenCapture.CaptureScreenshot(System.IO.Path.Combine(folderPath, screenshotName));
        }
    }
}
