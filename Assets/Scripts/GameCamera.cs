using UnityEngine;
using System.Collections;

public class GameCamera : MonoBehaviour
{
    public static GameCamera singleton { get; private set; }

    #region BODY
    //for defining position definition
    public Transform target;
    public float lerpSpeed = 2;

    private void Awake()
    {
        if (singleton != null)
        {
            Destroy(gameObject);
        }
        else
        {
            singleton = this;
        }
    }
    private void Update()
    {
        float best = GameManager.singleton.HighestHeight;
        Vector3 lerpTarget = new Vector3(target.position.x, best - 5f, target.position.z);
        transform.position = Vector3.Lerp(transform.position, lerpTarget, Time.deltaTime * lerpSpeed);
    }
    #endregion

    //#region TRAUMA
    ////for handling trauma (camera shake)
    //private float frequency = 25f;
    //public Vector3 maxAngularShake = Vector3.one * 2;
    //public float recoverySpeed = 1.5f;
    //public float traumaExponent = 2;

    ////triggered on collision with object by player
    //public void TriggerShake()
    //{
    //    StartCoroutine(Shake());
    //}
    //private IEnumerator Shake()
    //{
    //    //store the initial rotation for resetting on completion
    //    Quaternion startRotation = transform.rotation;

    //    //define a seed
    //    float seed = Random.value;

    //    //define the trauma
    //    float trauma = 1;

    //    //use trauma to define shake
    //    float shake = Mathf.Pow(trauma, traumaExponent);

    //    while (shake > 0)
    //    {
    //        //rotational shake
    //        transform.localRotation = Quaternion.Euler(new Vector3(
    //        maxAngularShake.x * (Mathf.PerlinNoise(seed + 3, Time.time * frequency) * 2 - 1),
    //        maxAngularShake.y * (Mathf.PerlinNoise(seed + 4, Time.time * frequency) * 2 - 1),
    //        maxAngularShake.z * (Mathf.PerlinNoise(seed + 5, Time.time * frequency) * 2 - 1)) 
                
    //        * shake) ;

    //        shake -= Time.deltaTime * recoverySpeed;

    //        yield return new WaitForEndOfFrame();
    //    }

    //    transform.rotation = startRotation;
    //}
    //#endregion
}
