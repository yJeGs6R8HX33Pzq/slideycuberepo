using UnityEngine;
using TMPro;
using System.Collections;
using UnityEngine.UI;

public class EncouragingTriangle : MonoBehaviour
{
    //the speech bubble of encouraging triangle
    public Image speechBubble;

    //the text in triangle speak to fill the bubble
    public TMP_Text realSpeech;
    //the default length of triangle speak bubble text

    //the translated text for encouraging triangle
    public TMP_Text translationSpeech;

    //a list of message in triangleSpeak
    private string[] triangleSpeak = new string[]
    {
        "akg",
        "ebj",
        "alo",
        "ywn",
        "qmp",
        "uip",
        "emp",
        "wma",
        "lpw",
        "pwo",
        "wpd"
    };

    //the english translations of triangleSpeak phrases
    private string[] encouragements = new string[]
    {
        "'better luck next time!'",
        "'good try!'",
        "'you're awesome.'",
        "'you can do it!'",
        "'hang in there!'",
        "'stay strong.'",
        "'you're amazing.'",
        "'keep your head up!'",
        "'keep climbing.'",
        "life is about the journey, not the destination.",
        "fortune favors the bold."
    };

    //called from GameManager to give encouragement
    public void Encourage()
    {
        StartCoroutine(GiveEncouragement());
    }

    //handles the activation of speech bubble, display of triangleSpeak message in the speech bubble, presentation of the translation text, and deactivation of those elements
    private IEnumerator GiveEncouragement()
    {
        //handles the random selection
        int selection = Random.Range(0, triangleSpeak.Length);

        //handles the triangle's realSpeech
        realSpeech.text = triangleSpeak[selection] + "!";

        //enables the speech bubble
        speechBubble.gameObject.SetActive(true);

        //initializes the target translation text
        string translation = "(Encouraging Triangle says " + encouragements[selection] + ")";

        //ensures the translationSpeech text is empty, then enables it
        translationSpeech.text = "";
        translationSpeech.gameObject.SetActive(true);

        //reveals the translation one character at a time
        foreach (char c in translation)
        {
            translationSpeech.text += c;
            yield return new WaitForSeconds(.0125f);
        }

        //pauses
        yield return new WaitForSeconds(2);

        //clears the translation speech
        translationSpeech.text = "";
        translationSpeech.gameObject.SetActive(false);

        //clears the realSpeech and deactivates speechBubble
        realSpeech.text = "";
        speechBubble.gameObject.SetActive(false);
    }
}
