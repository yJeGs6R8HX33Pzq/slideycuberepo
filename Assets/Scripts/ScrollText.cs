using UnityEngine;
using TMPro;

public class ScrollText : MonoBehaviour
{
    //for text display
    public TMP_Text text;

    //for determining positioning
    private RectTransform rect;
    private RectTransform canvasRect;
    private Vector2 uiOffset;


    private void Awake()
    {
        rect = GetComponent<RectTransform>();
        canvasRect = OverlayInterface.singleton.GetComponent<RectTransform>();
    }
    public void SetValue(string textToDisplay, bool isStreaking)
    {
        text.text = textToDisplay;

        if (isStreaking)
        {
            //adjust scale
            transform.localScale *= 1.5f;
        }
    }
    public void Place(Vector3 worldPos)
    {
        uiOffset = new Vector2(canvasRect.sizeDelta.x / 2f, canvasRect.sizeDelta.y / 2f);

        Vector2 ViewportPosition = Camera.main.WorldToViewportPoint(worldPos);
        Vector2 proportionalPosition = new Vector2(ViewportPosition.x * canvasRect.sizeDelta.x, ViewportPosition.y * canvasRect.sizeDelta.y);

        rect.localPosition = proportionalPosition - uiOffset;

        Destroy(gameObject, 1);
    }
}
