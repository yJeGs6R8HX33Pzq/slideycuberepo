using UnityEngine;

public static class CrossSceneData
{
    //stores a list of all datum (level profiles)
    public static Database database = Resources.Load<Database>("Database");

    //the current data, defaulted to the first data in the database
    public static Data current = database.datum[0];

    //this is set on scene load
    //in game scene, the current value is used for a variety of functions before setting value
    //e.g whether an instructional text should be displayed, or whether the EncouragingTriangle should send encouragement
    public static string prevScene;

    //a list of bools representing instructions for each level
    //on game scene load, if 
    public static bool[] instructions = new bool[]
    {
        false,
        false,
        false,
        false,
        false,
        false
    };


    public static Material[] playerMaterials = new Material[]
    {
        Resources.Load<Material>("1_Blue"),
        Resources.Load<Material>("2_Green"),
        Resources.Load<Material>("3_Yellow"),
        Resources.Load<Material>("4_Orange"),
        Resources.Load<Material>("5_Red"),
        Resources.Load<Material>("6_Purple"),
    };

    static Material currentMaterial = playerMaterials[0];
    public static Material GetCurrentMaterial
    {
        get
        {
            return currentMaterial;
        }
    }

    public static void SetMaterial(int val)
    {
        currentMaterial = playerMaterials[val];
    }

    //returns whether the current level is the last level
    //used to determine what the scoring format should be : all non-last levels of format 'Score' followed by ''Goal, and last level of format 'Score' followed by 'Highscore'
    //used because all non-last levels are finite and therefore have a different score text format
    public static bool isLast
    {
        get
        {
            return System.Array.IndexOf(database.datum, current) >= database.datum.Length - 1;
        }
    }

    //incremenets the current level
    public static void Increment()
    {
        int index = System.Array.IndexOf(database.datum, current);
        index += 1;

        //runs a max check and returns to zero if exceeds
        //occurs when called from LobbyManager

        if (index >= database.datum.Length)
        {
            index = 0;
        }

        current = database.datum[index];
    }

    //decrements the current level
    public static void Decrement()
    {
        int index = System.Array.IndexOf(database.datum, current);
        index -= 1;

        //runs a min check and returns to datum length - 1 if subceeds
        //occurs when called from LobbyManager

        if (index < 0)
        {
            index = database.datum.Length - 1;
        }

        current = database.datum[index];
    }
}
