using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI.Extensions;
public class GameManager : MonoBehaviour
{
    public static GameManager singleton { get; private set; }

    #region PLAYER
    //reference is cached for position tracking (Update), which is used for setting the camera position
    public Player player;
    #endregion

    #region BODY
    private void Awake()
    {
        //sets the singleton
        singleton = this;

        //retrieves the current level
        currentLevel = CrossSceneData.current;


        //time scale handling
        Time.timeScale = currentLevel.speed.start;

        //sets the isLast field
        isLast = CrossSceneData.isLast;

        //stores the level completion key and current status
        //used in scoring -> on level up
        key = currentLevel.id.ToString() + " complete";
        isComplete = PlayerPrefs.GetInt(key, 0) == 1 ? true : false;

        //sets the UI displays
        //called here and in UpdateScore on level up
        OnLoadLevel();

        //checks if the previous scene was game
        //only try encouragement if the prevScene was game (meaning the player just died), otherwise set the prevScene value to game
        if (CrossSceneData.prevScene == "Game")
        {
            TryEncouragement();
        }
        else
        {
            CrossSceneData.prevScene = "Game";
        }

        gradient._vertex1 = currentLevel.defaultColor_1;
        gradient._vertex2 = currentLevel.defaultColor_2;

        background.SetActive(false);
        background.SetActive(true);
    }
    private void Update()
    {
        //keeps track of the highest height attained
        //used for determining where the camera should be located (should centered on the highest height attained)
        //highestHeight retrieved from GameCamera class
        if (currentHeight > highestHeight)
        {
            highestHeight = currentHeight;
        }
    }

    //resets the score and text display values
    private void OnLoadLevel()
    {
        //resets the score (current and real)
        displayedScore = 0;
        realScore = 0;

        scoreValueText.text = "00.00 ";

        if (isLast)
        {
            highscore = PlayerPrefs.GetFloat("highscore", 0);

            objScoreText.text = "(Best : " + highscore + ")";
        }
        //otherwise, if the level has been completed, simply display complete, else display objective to format Goal : Level Goal
        else
        {
            objScoreText.text = "(of " + currentLevel.goal.ToString() + ")";
        }

        //if display not yet shown to completion, handle instruction display

        if (CrossSceneData.instructions[currentLevel.id - 1] == false)
        {
            StartCoroutine(HandleInstructionDisplay());
        }
    }

    //determines whether an instruction need be given, and if so, handles instruction display execution
    private IEnumerator HandleInstructionDisplay()
    {
        //initializes a counter
        float ctr = 0;

        //display or reveal (by character) instruction to display
        instructionsText.gameObject.SetActive(true);

        //gets the message
        string msg = msgs[currentLevel.id - 1];

        //displays the message

        if (CrossSceneData.prevScene == "Game")
        {
            instructionsText.text = msg;
        }
        else
        {
            foreach (char c in msg)
            {
                instructionsText.text += c;
                yield return new WaitForSeconds(.02f);
            }
        }

        //holds the message on screen for 5 seconds
        while (ctr < 8f)
        {
            ctr += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        //resets the counter
        ctr = 0;

        //fades the message out
        while (instructionsText.color != Color.clear)
        {
            instructionsText.color = Color.Lerp(Color.white, Color.clear, ctr);
            ctr += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        //resets the text object
        instructionsText.text = "";
        instructionsText.color = Color.white;
        instructionsText.gameObject.SetActive(false);
        CrossSceneData.instructions[currentLevel.id - 1] = true;
    }
    #endregion

    #region LEVEL DATA
    //the currently selected levelData, retrieved from CrossSceneData static class
    public Data currentLevel;

    #endregion

    #region MESSAGES
    //a list of instructions displayed on new level start
    private string[] msgs = new string[]
    {
        "Start by swiping screen to slide",
        "Tap once for shield",
        "Keep moving over vanishing blocks",
        "Prepare for liftoff...",
        "Last Level : Keep your eye on the prize!",
        "Reach for the stars!"
    };
    #endregion

    #region UI MANAGEMENT
    [Header("UI ELEMENTS")]
    //displays current score
    public TMP_Text scoreValueText;

    //text field top center screen for checkpoint display
    public TMP_Text checkPointText;

    //text field bottom right screen for instructions display
    public TMP_Text instructionsText;

    //the objective score display (... of 'x', where x is the level goal)
    //used in all non-last levels
    public TMP_Text objScoreText;

    //the highscore display
    //used on last level as a replacement for objScoreText
    public TMP_Text bestScoreText;

    //defines target color for alpha fade on the checkpoint text
    Color clearWhite = new Color(1, 1, 1, 0);

    #endregion

    #region CAMERA MANAGEMENT
    //height tracking used to set the position of the camera
    private float highestHeight;

    //getter for the height
    public float HighestHeight
    {
        get
        {
            return highestHeight;
        }
    }

    //getter for the current height
    private float currentHeight
    {
        get
        {
            return player.transform.position.y; 
        }
    }

    //getter for the current level color
    //gotten from GameCamera
    public Color GetResetColor_1
    {
        get
        {
            return currentLevel.defaultColor_1;
        }
    }

    public Color GetResetColor_2
    {
        get
        {
            return currentLevel.defaultColor_2;
        }
    }
    #endregion

    #region SCORING
    //the value currently displayed on screen
    //does not always reflect the real score - as the value is updated over time for visual effect by coroutine
    private float displayedScore;

    //the actual score
    private float realScore;

    //the highscore
    private float highscore;

    //local reference for whether the current level is the last level
    private bool isLast;

    //fields for level completion status
    private string key;
    private bool isComplete;

    [SerializeField] private UnityEngine.UI.Extensions.Gradient gradient;
    [SerializeField] private GameObject background;


    //called on ProceduralCube collision
    public void SetScore(float val)
    {
        //increments the real score to passed value, unless value plus score exceeds goal, in which case sets score to goal
        if (realScore + val > currentLevel.goal && !isLast)
        {
            realScore = currentLevel.goal;
        }
        else
        {
            realScore += val;
            realScore = (float)System.Math.Round(realScore, 2);
        }

        //handles the visual representation of score
        StartCoroutine(UpdateScoreDisplay());


        //updates the interpolant t, which represents progress by dividing the score by the goal
        //used to get the time scale as specified by the start and end values of the current levelData
        t = realScore / currentLevel.goal;

        //handles the time management
        Time.timeScale = GetSpeed;

        //handles level change if conditions are met
        if (realScore >= currentLevel.goal)
        {
            //if current is last level, do nothing
            if (!isLast)
            {
                //sets the current level to complete if not yet complete
                if (!isComplete)
                {
                    PlayerPrefs.SetInt(key, 1);
                }               

                //handles updating the current scene globally
                CrossSceneData.Increment();

                //retrieves the new currentLevel
                currentLevel = CrossSceneData.current;

                //determines the new isLast state
                isLast = CrossSceneData.isLast;

                //handles the color shift
                gradient._vertex1 = currentLevel.defaultColor_1;
                gradient._vertex2 = currentLevel.defaultColor_2;
                background.SetActive(false);
                background.SetActive(true);

                //handles the objective text reformatting if the new current level is last level
                OnLoadLevel();

                //handles the checkpoint text
                StartCoroutine(HandleCentralText());

                //handles the song
                StartCoroutine(SongShift());

                //handles new player abilities
                Player.singleton.SetPlayerAbilities();
            }
        }
    }
    //called in SetScore to adjust time scale
    private float GetSpeed
    {
        get
        {
            return Mathf.Lerp(currentLevel.speed.start, currentLevel.speed.end, t);
        }
    }

    //updates visual representation of score on screen - incrementing value up to the realScore
    //also handles the highscore representation if applicable
    private IEnumerator UpdateScoreDisplay()
    {
        yield return new WaitForSeconds(.2f);

        float ctr = 0f;
        float startScore = displayedScore;

        bool isUpdatingHighscore = realScore > highscore && isLast;
        if (isUpdatingHighscore)
        {
            highscore = realScore;
            PlayerPrefs.SetFloat("highscore", highscore);
        }

        while (displayedScore != realScore)
        {
            ctr += Time.deltaTime * 2;

            float thisVal = (float)System.Math.Round(Mathf.Lerp(startScore, realScore, ctr), 2);

            displayedScore = thisVal;
            scoreValueText.text = thisVal.ToString();

            if (isUpdatingHighscore)
            {
                objScoreText.text = "(Best : " + thisVal.ToString() + ")";
            }
            
            yield return new WaitForEndOfFrame();
        }
    }


    //called from SetScore on level change to handle checkpoint text display
    private IEnumerator HandleCentralText()
    {
        //display checkpoint if not yet last level
        if (!isLast)
        {
        checkPointText.text = "Checkpoint!";
        }
        //otherwise, display the game completion method
        else
        {
            checkPointText.text = "Game Complete :\nYou are a CUBE GOD!";
        }
        
        //central text handling
        checkPointText.gameObject.SetActive(true);
        checkPointText.color = Color.white;

        float ctr = 0f;

        while (checkPointText.color != clearWhite)
        {
            ctr += isLast?Time.deltaTime / 4 : Time.deltaTime;
            checkPointText.color = Color.Lerp(Color.white, clearWhite, ctr);
            yield return new WaitForEndOfFrame();
        }

        checkPointText.gameObject.SetActive(false);
        checkPointText.color = Color.white;
    }

    //decrements volume then updates the song
    private IEnumerator SongShift()
    {
        //caches the current volume value
        float vol = SoundController.singleton.song.volume;

        //caches the target volume
        float targetMusicVolume = SoundController.singleton.targetMusicVolume;

        //decrements the volume to zero
        while (vol > 0)
        {         
            vol -= Time.deltaTime * .15f;
            SoundController.singleton.song.volume = vol;

            yield return new WaitForEndOfFrame();
        }

        //calls the SetSong method
        SoundController.singleton.SetSong(CrossSceneData.current.song);

        //finalizes the volume at the exact value
        SoundController.singleton.song.volume = targetMusicVolume;
    }
    #endregion

    #region ENCOURAGING TRIANGLE
    [Header("ENCOURAGING TRIANGLE")]
    public EncouragingTriangle encouragingTriangle;
    public void TryEncouragement()
    {
        bool encourage = Random.Range(0f, 1f) < .35f ? true : false;
        if (encourage)
        {
            encouragingTriangle.Encourage();
        }
    }
    #endregion 

    #region SCENE MANAGEMENT
    public void ReturnToLobby()
    {
        Player.singleton.active = false;
        SceneManager.LoadScene("Lobby");
    }
    #endregion

    #region CUBE GENERATION
    //GameObject references
    [Header("GAMEOBJECT REFERENCES")]
    public GameObject proceduralCube;
    public GameObject heart;
    public GameObject spikes;
    public GameObject scrollText;

    //directional references
    Vector3[] dirs = new Vector3[]
    {
        Vector3.forward,
        Vector3.right,
        Vector3.back,
        Vector3.left
    };

    //material references

    [Header("STANDARD MATERIAL")]
    public Material whiteMaterial;

    [Header("VANISHING MATERIALS")]
    public Material vanishingMaterial;
    public Material vanishingMegaMaterial;
    public Material vanishingMovingMaterial;

    [Header("MEGA MATERIALS")]
    public Material megaUnlitMaterial;
    public Material megaLitMaterial;

    [Header("MOVING MATERIALS")]
    public Material movingUnlitMaterial;
    public Material movingLitMaterial;


    //interpolant t - set on ProceduralCube collision as a value between 0 and 1 signifying the progress of the player, represented by score / goal
    //used in all probability getters to select the appropriate value between the specified start and end value of the current levelData
    float t;

    //called on contact with a procedural cube
    public void GeneratePack(ProceduralCube p)
    {
        Vector3 dir;

        //handles direction selection
        int randIndex = Random.Range(0, 3);
        dir = dirs[randIndex];

        int distanceDelta;
        //handles position determination
        if (p.gameObject.tag == "Mega")
        {
            distanceDelta = 4;
        }
        else
        {
            distanceDelta = Random.Range(1, currentLevel.maxDelta);
        }


        //determines the target position
        float yPos = p.transform.position.y + 1;
        Vector3 pos = p.transform.position + dir * distanceDelta;
        Vector3 final = new Vector3(pos.x, yPos, pos.z);

        //performs instatiation
        GameObject c = Instantiate(proceduralCube, final, Quaternion.identity);

        //handles vanishing probability
        bool randVan = Random.Range(0f, 1f) < currentLevel.vanishingProbability;
        if (randVan)
        {
            c.GetComponent<ProceduralCube>().isVanishing = true;
            c.transform.localScale = new Vector3 (1, .5f, 1);
            c.transform.position += Vector3.up * .25f;

            BoxCollider col = c.GetComponent<BoxCollider>();
            col.center = Vector3.up * -.5f;
            col.size = new Vector3(.9f, 2, .9f);
        }

        //handles the mega probability
        bool randMega = Random.Range(0f, 1f) < currentLevel.megaProbability;
        if (randMega)
        {
            c.GetComponent<MeshRenderer>().material = megaUnlitMaterial;
            c.gameObject.tag = "Mega";         
        }

        //handles line renderer
        LineRenderer lr = p.gameObject.AddComponent<LineRenderer>();

        Vector3 slopedDir = (c.transform.position - p.transform.position).normalized;
        Vector3 offset = slopedDir / 2;

        Vector3 startPos = p.transform.position + new Vector3(offset.x, 0, offset.z);
        Vector3 endPos = c.transform.position - new Vector3(offset.x, 0, offset.z);

        lr.SetPosition(0, startPos);
        lr.SetPosition(1, endPos);

        lr.startWidth = .1f;
        lr.endWidth = .1f;
        lr.material = whiteMaterial;

        //handles isMoving after lineRenderer declaration so lineRenderer can be operated on by MovingDefinition if isMoving found to be true
        bool randMoving = false;
        if (!randMega)
        {
            randMoving = Random.Range(0f, 1f) < currentLevel.movingProbability ? true : false;
            if (randMoving)
            {
                MotionDefinition md = c.AddComponent<MotionDefinition>();
                c.GetComponent<MeshRenderer>().material = movingUnlitMaterial;
                c.tag = "Moving";
                md.Initialize(p.transform);
            }
        }

        //handles scoring reward
        //find a random value between min and max
        float rand = Random.Range(1f, 2f);

        //uses this float to generate a new float rounded to two decimal places
        float val = (float)System.Math.Round(rand, 2);

        //caches a references to player isStreaking value and adds a multiplier if the player is streaking
        bool isStreaking = Player.singleton.isStreaking;
        if (isStreaking)
        {
            float mult = Random.Range(1.2f, 2.2f);
            val = (float)System.Math.Round(val * mult, 2);
        }

        //calls the score handling method in the GameManger and passes the determined value
        SetScore(val);

        //handle scoring and ScrollText
        ScrollText s = Instantiate(scrollText, OverlayInterface.singleton.transform).GetComponent<ScrollText>();

        //handles the ScrollText
        string textToDisplay = "+" + val.ToString();
        s.SetValue(textToDisplay, isStreaking);
        s.Place(p.transform.position);

        //handles the material change on the current procedural cube to the activated material based on tag
        if (p.gameObject.tag == "Default")
        {
            p.GetComponent<MeshRenderer>().material = player.GetComponent<MeshRenderer>().material;
        }
        else if (p.gameObject.tag == "Mega")
        {
            p.GetComponent<MeshRenderer>().material = megaLitMaterial;
        }
        else if (p.gameObject.tag == "Moving")
        {
            p.GetComponent<MeshRenderer>().material = movingLitMaterial;
        }

        //handles platform object instantiation
        //selects a value for random object instantiation
        float objRand = Random.Range(0f, 1f);

        //uses the value to check for selections
        if (objRand < currentLevel.heartProbability)
        {
            if (Player.singleton.Lives < 3)
            {
                GameObject h = Instantiate(heart, final + Vector3.up, Quaternion.identity);
                h.transform.parent = c.transform;
            }
        }
        else if (objRand < currentLevel.heartProbability + currentLevel.spikeProbability)
        {
            GameObject sp = Instantiate(spikes, final, Quaternion.identity);
            sp.transform.parent = c.transform;
        }

        //destroys this ProceduralCube class identifier
        Destroy(p);
    }
    #endregion

    #region PLATFORM MANAGEMENT
    public void TriggerVanish(ProceduralCube p)
    {
        StartCoroutine(VanishPlatform(p.gameObject));
    }
    private IEnumerator VanishPlatform(GameObject g)
    {
        Collider col = g.GetComponent<Collider>();
        MeshRenderer rend = g.GetComponent<MeshRenderer>();
        Material mat = rend.material;

        col.enabled = false;

        if (g.tag == "Default")
        {
            rend.material = vanishingMaterial;
        }
        else if (g.tag == "Mega")
        {
            rend.material = vanishingMegaMaterial;
        }
        else if (g.tag == "Moving")
        {
            rend.material = vanishingMovingMaterial;
        }

        //can replace with a cascading if statement if more time types necessary
        float tm = g.tag == "Mega" ? 1.25f : 1;

        yield return new WaitForSeconds(tm);

        while (Physics.OverlapBox(g.transform.position, new Vector3 (.5f, .25f, .5f), Quaternion.identity, 1<<3).Length > 0)
        {
            yield return new WaitForEndOfFrame();
        }

        col.enabled = true;
        rend.material = mat;
    }
    #endregion

    #region TIME MANAGEMENT
    //called on play button press from pause screen
    public void OnResume()
    {
        Time.timeScale = GetSpeed;
    }

    //called from the pause button to pause time
    public void SetTimeScale(float val)
    {
        Time.timeScale = val;
    }
    #endregion

    #region AUDIO
    //serves as an access point to the sound controller singleton, because this method is called on button press
    public void PlayButton()
    {
        SoundController.singleton.PlayUIButton();
    }
    #endregion
}




