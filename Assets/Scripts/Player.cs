using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class Player : MonoBehaviour
{
    public static Player singleton;

    #region WORLD ORIENTATION
    Vector2 signedDirection;
    private Vector2 GetSignedDirection(Vector2 vec)
    {
        Vector2 newSignedDirection = new Vector2();
        float x;
        float y;

        if (vec.x > 0)
        {
            x = 1;
        }
        else
        {
            x = -1;
        }

        if (vec.y > 0)
        {
            y = 1;
        }
        else
        {
            y = -1;
        }

        newSignedDirection = new Vector2(x, y);
        return newSignedDirection;
    }
    //  -   -   -   -   -   -
    //signed direction matrix
    //-X, +Y = Vector3.forward
    //+X, +Y = Vector3.right
    //+X, -Y = Vector3.back
    //-X, -Y = Vector3.left
    //  -   -   -   -   -   -
    private Vector3 GetRealDirection()
    {
        Vector3 vec;

        float x = signedDirection.x;
        float y = signedDirection.y;

        if (x < 0 && y > 0)
        {
            vec = Vector3.forward;
        }
        else if (x > 0 && y > 0)
        {
            vec = Vector3.right;
        }
        else if (x > 0 && y < 0)
        {
            vec = Vector3.back;
        }
        else
        {
            vec = Vector3.left;
        }

        return vec;
    }
    Vector2 p1;
    Vector2 p2;
    #endregion

    #region BODY
    private void Awake()
    {
        if (singleton != null)
        {
            Debug.Log("Error! Player singleton is not null.");
            Destroy(gameObject);
        }
        else
        {
            singleton = this;
        }
    }
    public void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<Collider>();

        SetPlayerAbilities();

        //gets the material
        GetComponent<MeshRenderer>().material = CrossSceneData.GetCurrentMaterial;

        rb.velocity = Vector3.zero;

        //ensures no input is being pressed before movement begins.
        StartCoroutine(WaitOnInputRelease());
    }

    //determines what abilities should be set to active, depending on the current level
    public void SetPlayerAbilities()
    {
        int id = CrossSceneData.current.id;
        if (id > 1)
        {
            shieldActive = true;
        }
    }
    private bool hasCleared;
    private IEnumerator WaitOnInputRelease()
    {
        while (Input.touchCount > 0)
        {
            yield return new WaitForEndOfFrame();
        }
        hasCleared = true;
    }

    public void Update()
    {
        if (rb.velocity.y >= 30)
        {
            rb.velocity = Vector3.zero;
        }

        if (!isDashing)
        {
            rb.velocity -= Vector3.down * Physics.gravity.y * Time.deltaTime * 4f;    
        }

        //returns out of method if touch is on a UI element
        if (EventSystem.current.currentSelectedGameObject != null)
        {
            return;
        }

        if (!hasCleared)
        {
            return;
        }

        foreach (Touch t in Input.touches)
        {
            if (t.phase == TouchPhase.Began)
            {
                p1 = t.position;
            }

            else
            {
                if (t.phase == TouchPhase.Ended)
                {

                    p2 = t.position;

                    float dist = Vector2.Distance(p1, p2);
                    if (dist > 15f)
                    {
                        if (!isDashing)
                        {
                            signedDirection = GetSignedDirection(p2 - p1);
                            Vector3 realDirection = GetRealDirection();

                            float distMultiplier = Mathf.Round(dist / 120f);

                            TriggerDash(realDirection, distMultiplier);
                        }
                    }
                    else
                    {
                        if (shieldActive)
                        {
                            TryShield();
                        }
                    }
                }
            }
        }

    }
    #endregion

    #region PHYSICS
    public GameObject poof;
    private Rigidbody rb;
    private Collider col;


    private void OnCollisionEnter(Collision collision)
    {
        //rounds out the position to make sure is always exact
        transform.position = new Vector3(Mathf.Round(transform.position.x), transform.position.y, Mathf.Round(transform.position.z));
        sty = 0;

        isDashing = false;

        ProceduralCube p = collision.collider.GetComponent<ProceduralCube>();
        if (p != null)
        {
            //handles the MotionDefinition if applicable
            //(done before generate pack because OnContact finalizes the moving platform position.)
            MotionDefinition md = collision.collider.GetComponent<MotionDefinition>();
            if (md != null)
            {
                md.OnContact();
            }

            //handles procedural cube generation
            GameManager.singleton.GeneratePack(p);

            //handles poof particle effect
            GameObject pf = Instantiate(poof, p.transform.position + Vector3.up, Quaternion.identity);
            Destroy(pf, 2);

            //handles streak functionality
            if (!isStreaking)
            {
                currentStreak += 1;
                if (currentStreak > streakThreshold)
                {
                    isStreaking = true;
                }
            }

            //triggers vanishing if applicable
            if (p.isVanishing)
            {
                GameManager.singleton.TriggerVanish(p);
            }

            //sets the respawn point relative to this position
            rPoint = p.transform.position + Vector3.up * 3.5f;
        }
        else
        {
            if (isStreaking)
            {
                isStreaking = false;
            }
        }

        if (collision.collider.tag == "Mega")
        {
            rb.mass = .75f;
            SoundController.singleton.PlayBigJump();
        }
        else
        {
            rb.mass = 1;

            if (collision.collider.tag == "Default")
            {
                SoundController.singleton.PlayCompression();
            }
            else if (collision.collider.tag == "Moving")
            {
                SoundController.singleton.PlayChime();
                Vibration.VibrateNope();
            }
        }


        //if occupying the same x and z position
        if (transform.position.x == collision.transform.position.x && transform.position.z == collision.transform.position.z && transform.position.y + .4f < collision.transform.position.y)
        {

                transform.position = collision.transform.position + Vector3.up * .1f;
                rb.velocity = Vector3.zero;


        }
        else
        {
            float force = 1000;
            rb.AddForce(Vector3.up * force);

            //GameCamera.singleton.TriggerShake();
        }

        canDash = true;

    }

    private void OnTriggerEnter(Collider other)
    {
        //sets the gameinterface target heart to target status = true, increments the lives, destroys the object, and plays vibration
        if (other.tag == "Heart")
        {
            GameInterface.singleton.SetHeart(lives, true);
            lives += 1;         
            Destroy(other.gameObject);
            Vibration.VibratePop();
        }

        //checks if shielding, and if so, destroys the spikes and plays vibration, otherwise, runs the TryTerminate method
        if (other.tag == "Spikes")
        {
            Destroy(other.gameObject);
            if (isShield)
            {
                Vibration.VibratePeek();
            }
            else
            {
                TryTerminate();
            }
        }
    }

    private float sty = 0f;
    private void OnCollisionStay(Collision collision)
    {
        sty += Time.deltaTime;
        if (sty > .2f)
        {
        rb.AddForce(Vector3.up * 1000);
        }
    }

    #endregion

    #region STREAK
    public bool isStreaking;
    private int currentStreak;
    private int streakThreshold = 6;

    #endregion

    #region DASH
    public GameObject dashEffect;

    //represents whether the player is currently dasing
    private bool isDashing;
    private bool canDash = true;

    //the cumulative amount of mid-air dashes completed this jump
    //relavent when double dash is activates, as no dashing should be allow after the maximum dash amount

    public void TriggerDash(Vector3 dir, float distMultiplier)
    {
        if (canDash)
        {
            StartCoroutine(Dash(dir, distMultiplier));
        }
    }
    public IEnumerator Dash(Vector3 dir, float distMultiplier)
    {
        isDashing = true;
        canDash = false;

        rb.useGravity = false;
        rb.velocity = Vector3.zero;

        GameObject d = Instantiate(dashEffect, transform);
        d.transform.position = -.6f * dir + transform.position;
        d.transform.LookAt(transform.position);
        d.transform.parent = null;
        Destroy(d, 2f);

        //handle the audio
        SoundController.singleton.PlayDash();

        //handle the transform handling fields
        float ctr = 0f;
        Vector3 startPoint = transform.position;
        Vector3 endPoint = transform.position + dir * distMultiplier;

        while (transform.position != endPoint && isDashing)
        {
            ctr += Time.deltaTime * 6;
            transform.position = Vector3.Lerp(startPoint, endPoint, ctr);

            yield return new WaitForEndOfFrame();
        }

        if (!isDashing)
        {
        transform.position = new Vector3(Mathf.Round(transform.position.x), transform.position.y, Mathf.Round(transform.position.z));
        }

        rb.useGravity = true;
        isDashing = false;

        yield return null;
    }
    #endregion

    #region SHIELD
    public GameObject shield;

    public bool shieldActive;
    private bool isShield; 
    
    private void TryShield()
    {
        if (shieldActive && !isShield)
        {
            StartCoroutine(Shield());
        }
    }
    private IEnumerator Shield()
    {

        isShield = true;
        shield.SetActive(true);

        while (shield.transform.localScale.x < 2.5f)
        {
            shield.transform.localScale += Time.deltaTime * Vector3.one * 12;
            yield return new WaitForEndOfFrame();
        }

        //handle audio
        SoundController.singleton.PlayShield();

        float ctr = 0f;

        while (shield.GetComponent<MeshRenderer>().material.color.a > 0f)
        {
            shield.GetComponent<MeshRenderer>().material.color = new Color(1, 1, 1, 1 - ctr);
            ctr += Time.deltaTime * 4;
            yield return new WaitForEndOfFrame();
        }

        isShield = false;

        shield.SetActive(false);

        shield.transform.localScale = Vector3.one * .5f;
        shield.GetComponent<MeshRenderer>().material.color = Color.white;
    }
    #endregion

    #region HEALTH
    private Vector3 rPoint;
    private int lives;
    public int Lives
    {
        get
        {
            return lives;
        }
    }
    public bool active = true;
    private void OnBecameInvisible()
    {
        if (active)
        {
            TryTerminate();
        }
    }
    private void TryTerminate()
    {
        if (lives > 0)
        {
            GameInterface.singleton.SetHeart(lives - 1, false);
            lives -= 1;
            transform.position = rPoint;
            rb.velocity = Vector3.zero;
            GameManager.singleton.TryEncouragement();
        }
        else
        {
            SceneManager.LoadScene("Game");
        }
    }
    #endregion
}
