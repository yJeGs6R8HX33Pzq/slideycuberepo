using UnityEngine;
using System.Collections;

public class MotionDefinition : MonoBehaviour
{
    public Vector3 target;

    public Vector3 pointA;
    public Vector3 pointB;

    private LineRenderer lr;

    //takes the vector of the previous ProceduralCube
    public void Initialize(Transform t)
    {
        //assigns point a
        AssignPointA();

        //assigns point b
        AssignPointB(t.position);

        //assigns the lineRenderer components
        lr = t.GetComponent<LineRenderer>();

        //assigns the initial target to pointA (flipped on StartCoroutine)
        target = pointA;

        StartCoroutine(TargetedMove());
    }

    private void Update()
    {
        lr.SetPosition(1, transform.position);
    }

    #region POSITION ASSIGNMENT
    private void AssignPointA()
    {
        pointA = transform.position;
    }
    private void AssignPointB(Vector3 vec)
    {
        //determines the normalized vector
        Vector3 hVec = new Vector3(vec.x, transform.position.y, vec.z);
        Vector3 dir = (hVec - transform.position).normalized;

        //determines a random to or away bool
        int i = Random.Range(0f, 1f) < .5f ? 1 : -1;

        pointB = dir * -1 + transform.position;
    }
    #endregion


    //oscillates between the two points with a pause
    private IEnumerator TargetedMove()
    {
        Vector3 start;

        if (target == pointA)
        {
            start = pointA;
            target = pointB;
        }
        else
        {
            start = pointB;
            target = pointA;

        }

        float ctr = 0;

        while (transform.position != target)
        {
            transform.position = Vector3.Lerp(start, target, ctr);
            ctr += Time.deltaTime * 12;

            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(1f);

        StartCoroutine(TargetedMove());
    }

    public void OnContact()
    {
        transform.position = target;
        lr.SetPosition(1, transform.position);
        Destroy(this);
    }
}

