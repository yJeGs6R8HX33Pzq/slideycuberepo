using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using UnityEngine.UI.Extensions;

public class Lobby : MonoBehaviour
{
    public Transform plr;

    //displays the difficulty of the currently selected level
    public TMP_Text difficultyHeader;

    //displays completion status of the currently selected level
    public TMP_Text completionHeader;

    [SerializeField] private UnityEngine.UI.Extensions.Gradient gradient;
    [SerializeField] private GameObject background;

    private void Start()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;


        //sets the prevScene to Lobby
        CrossSceneData.prevScene = "Lobby";

        //sets all instructions statuses to false
        for (int i = 0; i < CrossSceneData.instructions.Length; i++)
        {
            CrossSceneData.instructions[i] = false;
        }

        //handles the UI
        UpdateUI();

        //update the player
        plr.GetComponent<MeshRenderer>().material = CrossSceneData.GetCurrentMaterial;

        bool musicIsOn = SoundController.singleton.song.enabled;
        if (musicIsOn)
        {
            musicIcon.SetActive(true);
            noMusicIcon.SetActive(false);
        }
        else
        {
            musicIcon.SetActive(false);
            noMusicIcon.SetActive(true);
        }
    }

    //for changing level selection
    public void OnArrowPress(int val)
    {
        //checks the value and increments or decrements the current level accordingly
        if (val == 1)
        {
            CrossSceneData.Increment();
        }
        else if (val == -1)
        {
            CrossSceneData.Decrement();
        }
        else
        {
            Debug.Log("Error! Input value is not valid!");
        }

        UpdateUI();
    }
    private void UpdateUI()
    {
        //retrieves the new level data
        Data currentLevel = CrossSceneData.current;

        //time scale handling
        Time.timeScale = currentLevel.speed.start;

        //sets the SoundControllers song source AudioClip
        SoundController.singleton.SetSong(currentLevel.song);

        //sets the difficulty text
        difficultyHeader.text = currentLevel.difficulty;

        //sets the completionHeader to best + highscore if is last level
        if (CrossSceneData.isLast)
        {
            completionHeader.text = "Best : " + PlayerPrefs.GetFloat("highscore", 0).ToString();
        }
        //otherwise, formats to completion status
        else
        {
            string key = currentLevel.id.ToString() + " complete";
            bool isComplete = PlayerPrefs.GetInt(key, 0) == 1 ? true : false;

            if (isComplete)
            {
                completionHeader.text = "complete";
            }
            else
            {
                completionHeader.text = "";
            }
        }

        //handle the background coloration
        gradient._vertex1 = currentLevel.defaultColor_1;
        gradient._vertex2 = currentLevel.defaultColor_2;

        background.SetActive(false);
        background.SetActive(true);

    }

    //called on start button press
    public void StartGame()
    {
        SceneManager.LoadSceneAsync("Game");
    }

    //takes an int and calls the CrossSceneData's SetMaterial method, passing the index as parameter

    public void SetPlayerMaterial(int val)
    {
        CrossSceneData.SetMaterial(val);
        plr.GetComponent<MeshRenderer>().material = CrossSceneData.GetCurrentMaterial;
    }

    #region AUDIO
    public GameObject musicIcon;
    public GameObject noMusicIcon;

    //serves as an access point to the sound controller singleton, which is DontDestroyOnLoad
    public void PlayUIButton()
    {
        SoundController.singleton.PlayUIButton();
    }

    public void OnMusicButton()
    {
        bool musicIsOn = SoundController.singleton.song.enabled;
        if (musicIsOn)
        {
            SoundController.singleton.song.enabled = false;
            musicIcon.SetActive(false);
            noMusicIcon.SetActive(true);
        }
        else
        {
            SoundController.singleton.song.enabled = true;
            musicIcon.SetActive(true);
            noMusicIcon.SetActive(false);
        }
    }
    #endregion

    #region DEBUG
    //for resetting all PlayerPrefs (debug)
    public bool isReset;
    private void Update()
    {
        //for debugging
        if (isReset)
        {
            foreach (Touch t in Input.touches)
            {
                //if you tap
                if (t.phase == TouchPhase.Began)
                {
                    Debug.Log("Reset Player Prefs!");
                    PlayerPrefs.DeleteAll();
                }
            }
        }
    }
    #endregion
}
