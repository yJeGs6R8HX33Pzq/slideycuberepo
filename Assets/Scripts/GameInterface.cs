using UnityEngine;
using UnityEngine.UI;

public class GameInterface : MonoBehaviour
{
    public static GameInterface singleton { get; private set; }

    //used for scollTexts from positioning
    private RectTransform canvasRect;
    public RectTransform GetCanvas()
    {
        return canvasRect;
    }


    #region HEALTH
    public Color heartFilled;
    public Color heartEmpty;
    public Image[] hearts;
    public Image[] Hearts
    {
        get
        {
            return hearts;
        }
    }
    public void SetHeart(int index, bool state)
    {
        if (state)
        {
            hearts[index].color = heartFilled;
        }
        else
        {
            hearts[index].color = heartEmpty;
        } 
    }
    #endregion

    #region BODY
    private void Awake()
    {
        singleton = this;
        canvasRect = GetComponent<RectTransform>();
    }
    #endregion
}
