using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour
{
    public static SoundController singleton { get; private set; }

    #region AUDIO SOURCES
    //music
    public AudioSource song;

    //retrieves on level up to determine volume of new song after fade
    public float targetMusicVolume = .2f;

    //sfx
    public AudioSource uiButton;
    public AudioSource gameButton;
    public AudioSource dash;
    public AudioSource compression;
    public AudioSource slam;
    public AudioSource shield;
    public AudioSource megaCompression;
    public AudioSource chime;
    #endregion

    #region BODY
    private void Awake()
    {
        if (singleton != null)
        {
            Destroy(gameObject);
        }
        else
        {
            singleton = this;
        }
    }
    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }
    #endregion

    #region PUBLIC METHODS
    //music
    public void SetSong(AudioClip clip)
    {
        //sets the clip
        song.clip = clip;

        //starts playing the song
        song.Play();
    }

    //sfx
    public void PlayUIButton()
    {
        uiButton.Play();
    }
    public void PlayGameButton()
    {
        gameButton.PlayDelayed(.6f);
    }
    public void PlayDash()
    {
        dash.Play();
    }
    public void PlayCompression()
    {
        compression.Play();
    }
    public void PlaySlam()
    {
        slam.Play();
    }
    public void PlayShield()
    {
        shield.Play();
    }
    public void PlayBigJump()
    {
        megaCompression.Play();
    }

    public void PlayChime()
    {
        chime.Play();
    }

    #endregion
}
