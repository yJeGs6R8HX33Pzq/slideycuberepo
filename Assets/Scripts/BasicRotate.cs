using UnityEngine;

public class BasicRotate : MonoBehaviour
{
    //rotates the object around the y axis at constant effect
    void Update()
    {
        transform.RotateAround(transform.position, transform.up, Time.deltaTime * 50f);
    }
}
