using UnityEngine;

public class OverlayInterface : MonoBehaviour
{
    public static OverlayInterface singleton { get; private set; }
    private void Awake()
    {
        singleton = this;
    }
}
