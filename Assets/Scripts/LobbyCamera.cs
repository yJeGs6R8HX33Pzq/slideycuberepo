using UnityEngine;

public class LobbyCamera : MonoBehaviour
{
    public float speed = 15f;
    private void Update()
    {
        transform.Rotate(0, Time.deltaTime * speed, 0);
    }
}
